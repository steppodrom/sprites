# Create Sprites

create png from svg with white background
`inkscape -z -b white -e container.png container.svg -w 64 -h 64`

create compressed sprite from png
`plantuml -encodesprite 16z container.png > Container.puml`

## Color

as i'm lazy i just use the Aws coloring stuff, so inject

```plantUML
K8SEntityColoring(Container)
!define Container(e_alias, e_label, e_techn) AWSEntity(e_alias, e_label, e_techn, $color, Container, Container)
!define Container(e_alias, e_label, e_techn, e_descr) AWSEntity(e_alias, e_label, e_techn, e_descr, $color, Container, Container)
!define ContainerParticipant(p_alias, p_label, p_techn) AWSParticipant(p_alias, p_label, p_techn, $color, Container, Container)
!define ContainerParticipant(p_alias, p_label, p_techn, p_descr) AWSParticipant(p_alias, p_label, p_techn, p_descr, $color, Container, Container)
```

in the sprite (relace $color with the color your sprite should have)
